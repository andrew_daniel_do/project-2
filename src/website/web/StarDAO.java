package website.web;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class StarDAO
{


	public Star findStar(String id)
	{
		
		Connection connection = null;
		ResultSet resultSet = null;
		Star result = null;
		PreparedStatement statement = null;
		

    	String driver = "com.mysql.jdbc.Driver";
    	String url = "jdbc:mysql://localhost:3306/";
    	String dbName = "moviedb";
    	String user = "andrew"; 
    	String password = "bg79jd7c";
    	String sql = "select * from stars where id = ?";

    	
        	
    	
		
    	try 
    	{
    	    Class.forName(driver).newInstance(); 
    	    connection = DriverManager.getConnection(url + dbName, user, password);
    	    statement = connection.prepareStatement(sql);
    	    statement.setString(1, id);
    	    
    	    resultSet = statement.executeQuery();
    	    while(resultSet.next())
			{
    	    	  result = new Star();
    	    	  result.setId(resultSet.getInt("id"));
    	    	  result.setFirstName(resultSet.getString("first_name"));
    	    	  result.setLastName(resultSet.getString("last_name"));
    	    	  result.setDob(resultSet.getString("dob"));
    	    	  result.setPhoto_url(resultSet.getString("photo_url"));            
				
				
			}
    	    
    	    
    	} 
    	catch (Exception e) 
    	{
    		System.out.println(e);
    	} 
    	finally 
    	{
    	    if (resultSet != null) try { resultSet.close(); } catch (SQLException ignore) {}
    	    if (statement != null) try { statement.close(); } catch (SQLException ignore) {}
    	    if (connection != null) try { connection.close(); } catch (SQLException ignore) {}
    	}
    	
    	return result;
		
	}
	public List<Star> findStarsFromMovies(String query) throws Exception
    {
        List<Star> result = new ArrayList<Star>();

        String driver = "com.mysql.jdbc.Driver";
        String url = "jdbc:mysql://localhost:3306/";
        String dbName = "moviedb";
        String user = "andrew"; 
        String password = "bg79jd7c";
        String sql = "select * from stars where id in (select star_id from stars_in_movies where"
                    + " movie_id in (select id from movies where title = ?));";

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;     
        
        try 
        {
            Class.forName(driver).newInstance(); 
            connection = DriverManager.getConnection(url + dbName, user, password);
            statement = connection.prepareStatement(sql);
            statement.setString(1, query);

            resultSet = statement.executeQuery();
            while(resultSet.next())
            {
                Star star = new Star();
                star.setId(resultSet.getInt("id"));
                star.setFirstName(resultSet.getString("first_name"));
                star.setLastName(resultSet.getString("last_name"));
                star.setDob(resultSet.getString("dob"));
                star.setPhoto_url(resultSet.getString("photo_url"));
                result.add(star);

            }


        } 
        catch (SQLException e) 
        {
            e.printStackTrace();
        } 
        finally 
        {
            if (resultSet != null) try { resultSet.close(); } catch (SQLException ignore) {}
            if (statement != null) try { statement.close(); } catch (SQLException ignore) {}
            if (connection != null) try { connection.close(); } catch (SQLException ignore) {}
        }


        

        return result;
    }
}
