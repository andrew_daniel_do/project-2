package website.web;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;

public class GenreDAO
{
	public List<Genre> genreList() throws Exception
	{
		Connection connection = null;
		ResultSet resultSet = null;
		
		List<Genre> result = new ArrayList<Genre>();
		
		try
		{
			Class.forName("com.mysql.jdbc.Driver");
			String url = "jdbc:mysql://localhost:3306/moviedb";
			connection = DriverManager.getConnection(url, "andrew", "bg79jd7c");
			Statement statement = connection.createStatement();
			resultSet = statement.executeQuery("select * from genres");
			
			while(resultSet.next())
			{
				Genre genre = new Genre();
				genre.setId(resultSet.getInt("id"));
				genre.setName(resultSet.getString("name"));
				
				result.add(genre);
				
			}
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		finally
		{
			if(result != null)
			{
				try
				{
					resultSet.close(); 
				}
				catch(Exception ex) 
				{
					
				};
			}
			if(connection != null)
			{
				try
				{
					connection.close();
				}
				catch(Exception ex)
				{
					
				};
			}
		}
		
		return result;
	}

	public List<Genre> findGenresFromMovie(String query) throws Exception
    {
        List<Genre> result = new ArrayList<Genre>();

        String driver = "com.mysql.jdbc.Driver";
        String url = "jdbc:mysql://localhost:3306/";
        String dbName = "moviedb";
        String user = "andrew"; 
        String password = "bg79jd7c";
        String sql = "select * from genres where id in (select genre_id from genres_in_movies where movie_id in (select id from movies where title = ?));";

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;     
        
        try 
        {
            Class.forName(driver).newInstance(); 
            connection = DriverManager.getConnection(url + dbName, user, password);
            statement = connection.prepareStatement(sql);
            statement.setString(1, query);

            resultSet = statement.executeQuery();
            while(resultSet.next())
            {
                Genre genre = new Genre();
                genre.setId(resultSet.getInt("id"));
                genre.setName(resultSet.getString("name"));
                result.add(genre);

            }


        } 
        catch (SQLException e) 
        {
            e.printStackTrace();
        } 
        finally 
        {
            if (resultSet != null) try { resultSet.close(); } catch (SQLException ignore) {}
            if (statement != null) try { statement.close(); } catch (SQLException ignore) {}
            if (connection != null) try { connection.close(); } catch (SQLException ignore) {}
        }


        

        return result;
    }
	 
	
}
