package website.web;
import java.io.IOException;
import java.sql.*;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

//import publisher.data.User;
//import publisher.data.UserDAO;

public class LoginServlet extends HttpServlet 
{
   
   // private RequestDispatcher jsp;
    /*
    public void init(ServletConfig config) throws ServletException 
    {
        ServletContext context = config.getServletContext();
        jsp = context.getRequestDispatcher("/jsp/login.jsp");
     }
*/
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) 
    throws ServletException, IOException 
    {
      
    	String email = req.getParameter("email");
    	String passWord = req.getParameter("password");

    	String driver = "com.mysql.jdbc.Driver";
    	String url = "jdbc:mysql://localhost:3306/";
    	String dbName = "moviedb";
    	String user = "root"; 
    	String password = "bg79jd7c";
    	String sql = "select * from customers where email = ? and password = ?"; // Not sure how the password column is named, you need to check/update it. You should leave those ? there! Those are preparedstatement placeholders.

    	Connection connection = null;
    	PreparedStatement statement = null;
    	ResultSet resultSet = null;
    	boolean login = false;

    	try 
    	{
    	    Class.forName(driver).newInstance(); // You don't need to call it EVERYTIME btw. Once during application's startup is more than enough.
    	    connection = DriverManager.getConnection(url + dbName, user, password);
    	    statement = connection.prepareStatement(sql);
    	    statement.setString(1, email);
    	    statement.setString(2, passWord);
    	    resultSet = statement.executeQuery();
    	    login = resultSet.first();
    	} 
    	catch (Exception e) 
    	{
    	    throw new ServletException("Login failed for some reason", e);
    	} 
    	finally 
    	{
    	    if (resultSet != null) try { resultSet.close(); } catch (SQLException ignore) {}
    	    if (statement != null) try { statement.close(); } catch (SQLException ignore) {}
    	    if (connection != null) try { connection.close(); } catch (SQLException ignore) {}
    	}

    	if (login) 
    	{
    		req.getSession().setAttribute("email", email); // I'd prefer the User object, which you get from DAO, but ala.
    		resp.sendRedirect("home"); // Redirect to home page.
    	}
    	else 
    	{
    		//req.setAttribute("message", "Unknown username/password, try again"); // This sets the ${message}
    		req.getRequestDispatcher("/jsp/login.jsp").forward(req, resp); // Redisplay JSP.
    	}
    }
    
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) 
    throws ServletException, IOException 
    {
       doGet(req, resp);

    }  
}