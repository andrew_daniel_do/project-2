package website.web;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import java.io.*;

public class DomParser
{	
	  public static void main(String[] args)

	{

	      try 
	      {	
	         File inputFile = new File("C:\\Users\\andre\\workspace\\website\\movies\\mains243.xml");
	         DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	         DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	         Document doc = dBuilder.parse(inputFile);
	         doc.getDocumentElement().normalize();
	         
	         System.out.println("Root element : " + doc.getDocumentElement().getNodeName());
	         NodeList nList = doc.getElementsByTagName("movies");
	         System.out.println("nList length: " + nList.getLength());
	         System.out.println("----------------------------");
	         
	        
	            Node nNode = nList.item(0);
	            System.out.println("\nCurrent Element : " + nNode.getNodeName());
	           
	            
	          //  if (nNode.getNodeType() == Node.ELEMENT_NODE) 
	          //  {
	               Element eElement = (Element) nNode;
	             //  System.out.println("movie : " + eElement.getAttribute("directorfilms"));
	               System.out.println("director : " + eElement.getElementsByTagName("directorfilms").item(0).getTextContent());
	             
	          //  }
	         
	      }
	      catch (Exception e) 
	      {
	         e.printStackTrace();
	      }
	   }
	}

