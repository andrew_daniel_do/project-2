package website.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

public class HoverServlet extends HttpServlet 
{

    protected void doGet(HttpServletRequest request,
        HttpServletResponse response) throws ServletException, IOException 
    {

            String id = request.getParameter("id");
            Entry entry = new EntryDAO().findSingleMovie(id);
           
            HoverData hoverData = new HoverData("entry", entry);

            
            String json = new Gson().toJson(hoverData);
            response.setContentType("application/json");
            response.getWriter().write(json);
    }
}
