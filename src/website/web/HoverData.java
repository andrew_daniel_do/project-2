package website.web;

public class HoverData 
{
	
	    private final String label;
	    private final Entry value;

	    public HoverData(String _label, Entry _value) 
	    {
	        super();
	        this.label = _label;
	        this.value = _value;
	    }

	    public final String getLabel() {
	        return this.label;
	    }

	    public final Entry getValue() {
	        return this.value;
	    }
	

}
