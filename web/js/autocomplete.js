$(document).ready(function() {
    $(function() {
            $("#title").autocomplete({     
            source : function(request, response) {
            $.ajax({
                    url : "autosearch",
                    type : "GET",
                    data : {
                            title : request.term
                    },
                    dataType : "json",
                    success : function(data) {
                            response(data);
                    }
            });
    }
});
});
});
