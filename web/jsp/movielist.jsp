<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<table>

 
<script src="//code.jquery.com/jquery-2.2.1.js"></script>
<script src="http://cdn.jsdelivr.net/qtip2/2.2.1/jquery.qtip.min.js"></script>

<script>
// Create the tooltips only when document ready
 $(document).ready(function()
 {
     // MAKE SURE YOUR SELECTOR MATCHES SOMETHING IN YOUR HTML!!!
     $('a').each(function() {
         $(this).qtip({
            content: {
                text: function(event, api) {
                    $.ajax({
                            url: 'hover', // URL to the JSON file
			                type: 'GET', // POST or GET
			                dataType: 'json', // Tell it we're retrieving JSON
			                data: {
			                    id: $(this).attr('id') // Pass through the ID of the current element matched by
			                    },
			                    
			                    })
                    .then(function(data) {
                    
                     var content = 'My name is ' + data.value.title;
                      api.set('content.text', content);
                    }, function(xhr, status, error) {
                        // Upon failure... set the tooltip content to error
                        api.set('content.text', status + ': ' + error);
                    });
        
                    return 'Loading...'; // Set some initial text
                }
            },
            position: {                             
                                at: 'bottom center', 
                                my: 'top center',
                                viewport: $(window),
                                effect: true
                        },

                        show: {
                                event: 'mouseover',
                                solo: true 
                        },
                        
                        hide: 'unfocus',
                        fixed: true,
                        style: {
                               tip: true, // Give it a speech bubble tip
					            border: {
					               width: 3, 
					               radius: 8, 
					               color: '#646358'
					            },
					            title: {
					               color: '#fff'
					            },
					            width: 600
                        }
         });
     });
 });
 </script>

 
  <c:forEach var="movie" items="${movies}">
    <tr>
      		
      <td>${movie.id}</td>
      <td><a href="hover?id=${movie.id}"> ${movie.title}</td>
      <td>${movie.year}</td>
      <td>${movie.director}</td>
    
      <c:forEach var="star" items="${movie.starList}">
    	   <td><a href="star?query=${star.id}"> ${star.firstName}${star.lastName}</td>
       </c:forEach>
       <c:forEach var="genre" items="${movie.genreList}">
    	   <td>${genre.name}</td>
       </c:forEach>
      
      <td><img src ="${movie.banner}"></td>
      <td><a href="${movie.trailer}">trailer</a></td>         
    
    </tr>
  </c:forEach>

</table>