<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="./css/jquery.qtip.min.css" type="text/css" media="screen"/>
<title>AJAX Tool tip using qTip2 jQuery plugin</title>
<style>
.qtip-content {
        border: 1px solid grey;
}

.student-name {
        color: blue;
        cursor: pointer;
}

.name-style {
        font-weight: bold;
        font-size: 14px
}

.style {
        font-weight: bold;
}
</style>
</head>
<body>
<h2>Qtip2 Demo With Ajax Call</h2>

 

<script src="//code.jquery.com/jquery-2.2.1.js"></script>
<script src="http://cdn.jsdelivr.net/qtip2/2.2.1/jquery.qtip.min.js"></script>

<script>
// Create the tooltips only when document ready
 $(document).ready(function()
 {
     // MAKE SURE YOUR SELECTOR MATCHES SOMETHING IN YOUR HTML!!!
     $('a').each(function() {
         $(this).qtip({
            content: {
                text: function(event, api) {
                    $.ajax({
                            url: 'hover', // URL to the JSON file
			                type: 'GET', // POST or GET
			                dataType: 'json', // Tell it we're retrieving JSON
			                data: {
			                    id: $(this).attr('id') // Pass through the ID of the current element matched by
			                    },
			                    
			                    })
                    .then(function(data) {
                    
                     var content = 'My name is ' + data.value.title;
                      api.set('content.text', content);
                    }, function(xhr, status, error) {
                        // Upon failure... set the tooltip content to error
                        api.set('content.text', status + ': ' + error);
                    });
        
                    return 'Loading...'; // Set some initial text
                }
            },
            position: {                             
                                at: 'bottom center', 
                                my: 'top center',
                                viewport: $(window),
                                effect: true
                        },

                        show: {
                                event: 'mouseover',
                                solo: true 
                        },
                        
                        hide: 'unfocus',
                        fixed: true,
                        style: {
                               tip: true, // Give it a speech bubble tip
					            border: {
					               width: 3, 
					               radius: 8, 
					               color: '#646358'
					            },
					            title: {
					               color: '#fff'
					            },
					            width: 600
                        }
         });
     });
 });
 </script>

</body>
</html>