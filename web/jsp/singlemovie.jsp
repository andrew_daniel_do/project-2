<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>


<table>
 
 <a href="home">Home</a>



    <tr>
     
      <td>${choice.id}</td>
      <td>${choice.title}</td>
      <td>${choice.year}</td>
      <td>${choice.director}</td>
      <c:forEach var="star" items="${choice.starList}">
    	   <td><a href="star?query=${star.id}"> ${star.firstName}${star.lastName}</td>
       </c:forEach>
        <c:forEach var="genre" items="${choice.genreList}">
    	   <td>${genre.name}</td>
       </c:forEach>
    <td>  <img src ="${choice.banner}"></td>
      <td><a href="${choice.trailer}">trailer</a></td>         
    
    </tr>


</table>
