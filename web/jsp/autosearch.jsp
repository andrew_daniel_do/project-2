<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Autocomplete in java web application using Jquery and JSON</title>
<script src="//code.jquery.com/jquery-2.2.1.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
<!-- User defied css -->
<link rel="stylesheet" href="style.css">
</head>
 
<body>

<script>
$(document).ready(function() {
    $(function() {
            $("#title").autocomplete({     
            source : function(request, response) {
            $.ajax({
                    url : "autosearch",
                    type : "GET",
                    data : {
                            title : request.term
                    },
                    dataType : "json",
                    success : function(data) {
                            response(data);
                    }
            });
    }
});
});
});
</script>

<script type="text/javascript">
// Using jQuery.

$(function() {
    $('form').each(function() {
        $(this).find('input').keypress(function(e) {
            // Enter pressed?
            if(e.which == 10 || e.which == 13) {
                this.form.submit();
            }
        });

        $(this).find('input[type=submit]').hide();
    });
});
</script>

 
<form>

<input type="text" name="title" id="title"/> 
<input type="submit"/>

</form>
</body>
</html>